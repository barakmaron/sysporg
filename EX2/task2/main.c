#include <stdio.h>
#include <stdlib.h>

int findNumLines(FILE* opnedfile);
void findLine(FILE* opnedfile, int rand);

int main(int argc,char** argv)
{
	FILE* opnedfile = NULL;
	int rnd,nl;
	if(argc == 2)
	{
		opnedfile=fopen(argv[1], "r");//opning a file in read mode
		if(opnedfile != NULL)
		{
			nl = findNumLines(opnedfile);
			rnd= rand() % (nl+1);
			findLine(opnedfile,rnd);
		}
		else
		{
			printf("cant open the file plase try again!\n");
		}
	}
	else
	{
		printf("You didnt enter a file to open, enter a file in argv\n");
	}
	return 0;	
}

int findNumLines(FILE* opnedfile)
{
	int nl=0;
	char c=getc(opnedfile);
	while(c!=EOF)
	{
	    if(c=='\n')
		{
	    	nl++;
		}
	    c=getc(opnedfile);
	}
	rewind(opnedfile);
	return nl;
}

void findLine(FILE* opnedfile, int rand)
{
	int nl=0;
	char c=getc(opnedfile);
	char line[1024];
	while(c!=EOF)
	{
	    if(c=='\n')
		{
	    	nl++;
			if(nl == rand)
			{
				fgets(line, 1024, opnedfile);
				printf("%s",line);
				return;
			}
		}
	    c=getc(opnedfile);
	}
}
