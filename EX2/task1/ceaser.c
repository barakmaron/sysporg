#include "ceaser.h"
#include <string.h>

char shift_letter(char letter, int offset)
{
	if(letter == 'a'&&offset<0)
	{
		return ('z' - (offset * (-1)));
	}
	else if(letter == 'z'&&offset>0)
	{
		return 'a' + (offset - 1);
	}
	return ((letter - 'a' + offset) % 26) + 'a';
}

char * shift_string(char * input, int offset)
{
	int length = sizeof input +1;
	int i;
	char encrypted[length];

	for (i = 0; i < length; ++i)
		encrypted[i] = shift_letter(input[i],offset);
	encrypted[length+1] = (char)0;
	strcpy(input,encrypted);
	return input;
}
