#include "srv.h"

int main()
{
	pid_t sonID;
	int file;
	int status;
	/*close(1);*/
	file = open("output1.txt", O_WRONLY|O_CREAT,S_IRWXU);
	sonID = fork();	
	if(sonID >= 0)
	{
		if(sonID == 0)
		{
			waitpid(1,&status,0);
		}
		else
		{
			printf("\nDispatching");
			//signal
			kill(sonID, 15);
			printf("\nDispatched\n");			
		}
	}

	return 0;
}

