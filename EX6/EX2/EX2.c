#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/ioctl.h>

void quit(int signum) 
{
	int file;
	char char1[1024];
	printf("\nDo you want to exit? ");
	scanf("%s",char1);
	if(char1[0] == 'y'||char1[0] == 'Y')
	{
		kill(getpid(),9);
	}
	/*else
	{
		close(1);
		file = open("output1.txt", O_WRONLY|O_CREAT,S_IRWXU);
		printf("%s",char1);
		close(file);
		return;
	}*/
}

int main() 
{
	int i;
	signal(2, quit);
	signal(3, quit);
	signal(15, quit);
	for(i=0;i<2000000000000;i+=1000000)
	{
		printf("I'm still alive i=%d",i);
	}
	exit(EXIT_SUCCESS);
	return(0);
}