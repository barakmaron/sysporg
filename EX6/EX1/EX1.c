#include "EX1.h"

int main()
{
	pid_t sonID;
	int file;
	char * const tail[] = {"tail","-n 100","output1.txt",NULL};
	sonID = fork();	
	if(sonID >= 0)
	{
		if(sonID == 0)
		{
			close(1);
			file = open("output1.txt", O_WRONLY|O_CREAT,S_IRWXU);			
			while(1)
			{
				printf("I'm still alive\n");
			}
			exit(0);
		}
		else
		{
			sleep(1);
			printf("\nDispatching");
			//signal
			kill(sonID, 9);
			execvp(tail[0],tail);
			printf("\nDispatched\n");
			
		}
	}

	return 0;
}

