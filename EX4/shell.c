#include "shell.h"

int main()
{
	cmdLine * pCmdLine;
	char buf[PATH_MAX];
	char arg[2048];
	pid_t sonID;
	int status;
	char history[15][2048];
	int i=0,temp;
	do 
	{
		getcwd(buf,PATH_MAX);
		printf("%s$ ", buf);
		fgets(arg,2048,stdin);
		pCmdLine = parseCmdLines(arg);
		if(arg[0] == '!')
		{
			temp = (int)arg[1] - 48;
			if(arg[2]>='0'&&arg[2]<='5')
			{
				temp *= 10;
				temp = (int)arg[2] - 48;
			}
			strcpy(arg,history[temp]);
			pCmdLine = parseCmdLines(arg);
		}
		if(chackIf(arg,pCmdLine))
		{
			sonID = fork();
			if(sonID >=0)
			{
				if(sonID == 0)
				{
					execute(pCmdLine);
				}
				else
				{
					if(pCmdLine->blocking)
					{
						waitpid(sonID, &status, 0);
					}
					else
					{
						wait(&status);
					}
				}
			}
		}
		if(i==15)
		{
			for(i=0;i<14;i++)
			{
				strcpy(history[i],history[i+1]);
			}
			i=15;
		}
		strcpy(history[i],arg);
		i++;		
	}while(1);
	freeCmdLines(pCmdLine);
	return 0;
}

void execute(cmdLine *pCmdLine)
{
	int err;
	err = execvp(pCmdLine->arguments[0],pCmdLine->arguments);
	if(err == -1)
	{
		perror(pCmdLine->arguments[0]);
	}
}

int chackIf(char* arg,cmdLine *pCmdLine)
{
	int i;
	if(!strcmp(pCmdLine->arguments[0],"quit"))
	{
		exit(0);
		return 0;
	}
	else if(arg[0] == 'c' && arg[1] == 'd')
	{
		if(chdir(pCmdLine->arguments[1]))
		{
			perror("cd");
		}
		return 0;
	}
	else if(!strcmp(pCmdLine->arguments[0],"myecho"))
	{
		for(i=7;i<strlen(arg);i++)
		{
			printf("%c", arg[i]);
		}
		return 0;
	}
	return 1;
}