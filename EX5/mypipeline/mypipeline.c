#include "mypipeline.h"

#define IN 0
#define OUT 1

int main()
{
	pid_t sonID;
	pid_t sonID2;
	int status,status2;
	int err;	
	int fd[2];
	char * const ls[] = {"ls","-l",NULL};
	char * const tail[] = {"tail","-n 2",NULL};

	pipe(fd);

	sonID = fork();	
	if(sonID >= 0)
	{
		if(sonID == 0)
		{	
			close(1);		
			dup(fd[OUT]);
			close(fd[OUT]);
			err = execvp(ls[0],ls);
			if(err == -1)
			{
				perror("");
			}
		}
		else
		{
			close(fd[OUT]);
			sonID2 = fork();
			if(sonID2 >= 0)
			{
				if(sonID2 == 0)
				{
					close(0);		
					dup(fd[IN]);
					close(fd[IN]);
					err = execvp(tail[0],tail);
					if(err == -1)
					{
						perror("");
					}
				}
				else
				{
					close(fd[IN]);
					waitpid(sonID, &status, 0);
					waitpid(sonID2, &status2, 0);
				}	
			}
		}
	}
	return 0;
}

