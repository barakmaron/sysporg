#include "pipes.h"

#define IN 0
#define OUT 1

int main()
{
	pid_t sonID;
	int fd[2];
	char msg[]="magshimim";
	char ret[10];
	pipe(fd);

	sonID = fork();

	if(sonID >=0)
	{
		if(sonID == 0)
		{
			close(fd[IN]);
			write(fd[OUT], msg, sizeof(msg));
			exit(0);
		}
		else
		{
			close(fd[OUT]);
			read(fd[IN], ret, sizeof(ret));
			printf("%s\n", ret);
		}
	}
	return 0;
}