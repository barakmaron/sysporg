#include  "helper.h"
#include <unistd.h>
#include <sys/sysinfo.h>
#include  <sys/syscall.h>

typedef void(* fun)(void);

void sys1();
void sys2();
void sys3();

int main()
{
	char buf[1024] = "\nHello\n this are your options\n (1) for exit\n (2) for geting user id\n (3) for printig folder name\n plase enter your choose: ";
	char buf1[2] = " ";
	fun func[3];
	func[0] = (fun)&sys1;
	func[1] = (fun)&sys2;
	func[2] = (fun)&sys3;
	while(1)
	{
		syscall(4, 1, buf, slen(buf));
		syscall(3, 0, buf1, 1024);
		func[((int)buf1[0]-48)-1]();
	}
	return 0; 
}

void sys1()
{
	syscall(1, 0);
}

void sys2()
{
	uid_t n;
	syscall(4, 1, "\n The user id is: ", sizeof("\n The user id is: "));
	n = syscall(20);
	syscall(4, 1, __itoa(n), sizeof(n));
}

void sys3()
{
	char buf1[170]= "Hello\n this are your options\n (1) for exit\n";
	syscall(183,buf1);
	syscall(4,1,buf1,sizeof(buf1));
}