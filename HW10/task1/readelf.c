#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <elf.h>
#include <sys/mman.h>
#include <stddef.h>

void shdr_parse(void *ptr)
{
    Elf32_Shdr *shdr_ptr = NULL;
    Elf32_Ehdr *Elf32_ptr = (Elf32_Ehdr *)ptr;
    int n = Elf32_ptr->e_shnum;
    char *shstrtab = NULL;
	int i = 0;
    shdr_ptr = (Elf32_Shdr *)(ptr + Elf32_ptr->e_shoff);
    while (n--) {
        if (shdr_ptr->sh_type == SHT_STRTAB) 
        {
            shstrtab = (char *)(ptr + shdr_ptr->sh_offset);
            if (!strcmp(&shstrtab[shdr_ptr->sh_name], ".shstrtab"))
                break;
        }
        shdr_ptr++;
    }

    shdr_ptr = (Elf32_Shdr *)(ptr + Elf32_ptr->e_shoff);
    n = Elf32_ptr->e_shnum;
    while (n--) {
        printf("[%02d] %s\t\t0x%x\t0x%x\t0x%x\n", i++, 
                &shstrtab[shdr_ptr->sh_name], shdr_ptr->sh_addr, 
                shdr_ptr->sh_offset, shdr_ptr->sh_size);
        shdr_ptr++;
    }
}

int main()
{
    int fd_src;
    //size_t len = 0;
    size_t filesize = 0;
    void *ptr = NULL;
    fd_src = open("a.out", O_RDONLY);
    filesize = lseek(fd_src, 0, SEEK_END);
    ptr = mmap(0, filesize, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd_src, 0);
	printf("index:  name:\t\taddress:\toffset:\tsize:\n");
	shdr_parse(ptr);
    close(fd_src);

    return 0;
}