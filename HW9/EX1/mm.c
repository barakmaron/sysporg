#include "mm.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <syscall.h>

p_block first = NULL;
p_block last = NULL;
int count = -1;

void * malloc(size_t size)
{
	p_block p,current = first;
	void * ret;
	int i,j = count;

	for(i=0;i < count && current != last;i++)
	{
		if(current->size == size && current->free == 1)
		{
			current->free = 0;
			return current;
		}
		else
		{
			current = current->next;					
		}
	}

	if(last != NULL && last->free)
	{
		sbrk((size-last->size));
		last->free = 0;
		return last;
	}

	ret = sbrk(sizeof(p->size) + size);

	if(ret == (void *)-1)
	{
		perror("malloc");
		return NULL;
	}
	else
	{
		p = ret;
		p->size = size;
		p->free = 0;
		p->next = NULL;
		last = p;
		if(first == NULL)
		{
			first = p;
		}
		else
		{
			last->next = p;
			last = p;
			count++;
		}
		return ret;		
	}
}

void free(void * ptr)
{
	p_block temp;
	if(last == ptr && last != first)
	{
		sbrk(-1*(sizeof(last)));
	}
	else
	{
		temp = ptr;
		temp->free = 1;
	}
}


void * realloc(void * ptr,size_t size)
{
	void * ret;
	if(last->free && size >= last->size)
	{
		sbrk(size - last->size);
		last->free = 0;
		return last;
	}
	else if(last->free && size < last->size)
	{
		sbrk(-1*(last->size - size));
		last->free = 0;
		return last;
	}
	ret = malloc(size);
	return ret;
}