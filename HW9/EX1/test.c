#include "mm.h"
#include <stdio.h>
#include <string.h>


int main(int argc, char ** argv)
{
	char * str = malloc(10);
	void * ptr = (void *) str;
	void * ptr2;
	
	strcpy(str,"TEST!\0");
	free(str);
	printf("free: OK!\n");

	str = malloc(15);

	if ((void*) str != ptr)
	{
		printf("malloc() should've used the freed memory! old: %p new: %p\n",ptr,str);
		return 1;
	}

	ptr2 = realloc(str,10);

	malloc(10);
	ptr = realloc(str,20);

	if (ptr2 == ptr)
	{
		printf("realloc() should return a new memory block when expanding memory!\n");
		return 2;
	}
	printf("all good in the hood :)\n");
	return 0;
}