#include "shell.h"

#define IN 0
#define OUT 1

int main()
{
	cmdLine * pCmdLine;
	char buf[PATH_MAX];//path
	char arg[2048];//comand
	pid_t sonID;//fork
	int status;//wait
	char history[15][2048];//history
	int i=0,temp;//history
	do 
	{
		getcwd(buf,PATH_MAX);
		printf("%s$ ", buf);
		fgets(arg,2048,stdin);
		pCmdLine = parseCmdLines(arg);
		///////////////////////////////////////history///////////////////////////////////////
		if(arg[0] == '!')
		{
			temp = (int)arg[1] - 48;
			if(arg[2]>='0'&&arg[2]<='5')
			{
				temp *= 10;
				temp = (int)arg[2] - 48;
			}
			strcpy(arg,history[temp]);
			pCmdLine = parseCmdLines(arg);
		}
		///////////////////////////////////////history///////////////////////////////////////
		if(chackIf(arg,pCmdLine))
		{
			sonID = fork();
			if(sonID >=0)
			{
				if(sonID == 0)
				{
					if(pCmdLine->inputRedirect != NULL || pCmdLine->outputRedirect != NULL)
					{
						Redirection(pCmdLine);
					}
					else
					{
						execute(pCmdLine);
					}					
				}
				else
				{
					if(pCmdLine->blocking)
					{
						waitpid(sonID, &status, 0);
					}
					else
					{
						wait(&status);
					}
				}
			}
		}
		if(i==15)
		{
			for(i=0;i<14;i++)
			{
				strcpy(history[i],history[i+1]);
			}
			i=15;
		}
		strcpy(history[i],arg);
		i++;		
	}while(1);
	freeCmdLines(pCmdLine);
	return 0;
}

void execute(cmdLine *pCmdLine)
{
	int err;
	err = execvp(pCmdLine->arguments[0],pCmdLine->arguments);
	if(err == -1)
	{
		perror(pCmdLine->arguments[0]);
	}
}

int chackIf(char* arg,cmdLine *pCmdLine)
{
	pid_t sonID,sonID2;//fork
	int status,status2;//wait
	int i,fd[2],err;
	if(!strcmp(pCmdLine->arguments[0],"quit"))
	{
		exit(0);
		return 0;
	}
	else if(arg[0] == 'c' && arg[1] == 'd')
	{
		if(chdir(pCmdLine->arguments[1]))
		{
			perror("cd");
		}
		return 0;
	}
	else if(!strcmp(pCmdLine->arguments[0],"myecho"))
	{
		for(i=7;i<strlen(arg);i++)
		{
			printf("%c", arg[i]);
		}
		return 0;
	}
	else if(strchr(arg , '|'))
	{
		pipe(fd);
		sonID = fork();
		if(sonID >= 0)
		{
			if(sonID == 0)
			{	
				close(1);		
				dup(fd[OUT]);
				close(fd[OUT]);
				if(pCmdLine->inputRedirect != NULL || pCmdLine->outputRedirect != NULL)
				{
					Redirection(pCmdLine);
				}
				else
				{
					err = execvp(pCmdLine->arguments[0],pCmdLine->arguments);
				}
				if(err == -1)
				{
					perror("");
				}
			}
			else
			{
				close(fd[OUT]);
				sonID2 = fork();
				if(sonID2 >= 0)
				{
					if(sonID2 == 0)
					{
						close(0);		
						dup(fd[IN]);
						close(fd[IN]);
						if(pCmdLine->next->inputRedirect != NULL || pCmdLine->next->outputRedirect != NULL)
						{
							Redirection(pCmdLine->next);
						}
						else
						{
							err = execvp(pCmdLine->next->arguments[0],pCmdLine->next->arguments);
						}
						if(err == -1)
						{
							perror("");
						}
					}
					else
					{
						close(fd[IN]);
						wait(&status);
						wait(&status2);
					}	
				}
			}
		}
		return 0;
	}
	///////////////////////////////////////pipe_chack////////////////////////////////////
	return 1;
}

void Redirection(cmdLine *pCmdLine)
{
	int desctiptor,desctiptor2;
	if(pCmdLine->outputRedirect != NULL && pCmdLine->inputRedirect != NULL)
	{
		close(1);
		close(0);
		desctiptor = open(pCmdLine->inputRedirect, S_IROTH);
		desctiptor2 = open(pCmdLine->outputRedirect, O_WRONLY);
		dup2(IN,desctiptor);
		dup2(OUT,desctiptor2);
		execute(pCmdLine);
	}
	else if(pCmdLine->inputRedirect != NULL)
	{
		close(0);
		desctiptor = open(pCmdLine->inputRedirect, S_IROTH);
		dup2(IN,desctiptor);
		execute(pCmdLine);
	}
	else if(pCmdLine->outputRedirect != NULL)
	{
		close(1);
		desctiptor = open(pCmdLine->outputRedirect, O_WRONLY);
		dup2(OUT,desctiptor);
		execute(pCmdLine);
	}
	
}